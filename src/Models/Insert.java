/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author  Juan Carlos Quiroz Aguilar
 */
public class Insert {
    
    public void InsertAlmacenamiento(String Nombre, int Cantidad){
        
        
    try {
        Conexion con = new Conexion();
         Statement st = con.conecta().createStatement();
         PreparedStatement pst = (PreparedStatement) con.conecta().prepareStatement("INSERT INTO almacenalimentos(Nombre,Cantidad) VALUES (?,?)");
         pst.setString(1, Nombre);
         pst.setInt(2, Cantidad);
         pst.execute();
         
         
            } catch (SQLException ex) {
            System.out.println(ex.getErrorCode() + "Error en la ejecución:" + " " + ex.getMessage()); 
        }
}
}

