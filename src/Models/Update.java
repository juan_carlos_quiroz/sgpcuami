/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author  Juan Carlos Quiroz Aguilar
 */
public class Update {
        public void  ActualizarAlimento(String Nombre, int Cantidad) throws SQLException{
        Conexion con = new Conexion();
        Statement st = con.conecta().createStatement();
        
        
        
        PreparedStatement prest = (PreparedStatement) con.conecta().prepareStatement("UPDATE almacenalimentos SET Cantidad = ? WHERE Nombre = ?");
        prest.setString(2, Nombre);
        prest.setInt(1, Cantidad);
        
        prest.executeUpdate();
    }
}
