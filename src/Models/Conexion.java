/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author  Juan Carlos Quiroz Aguilar
 */
public class Conexion {
    public Connection con = null;

    public Conexion() {
        conecta();
    }

    public Connection conecta() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost/sgpcuami", "root", "");
        } catch (ClassNotFoundException error) {
            JOptionPane.showMessageDialog(null, "error en el controlador" + error.getMessage());

        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "error en la conexion" + error.getMessage());
        }

        return con;
    }

    Statement createStatement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
