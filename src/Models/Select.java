/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author  Juan Carlos Quiroz Aguilar
 */
public class Select {
    
    public ResultSet  ConsultPedidos() throws SQLException{
        Conexion con = new Conexion();
        Statement st = con.conecta().createStatement();
        
        ResultSet rs = st.executeQuery("SELECT * FROM pedidos ");
        
        return rs;
        
    } 
    
    
     public ResultSet  ListaMenu() throws SQLException{
        Conexion con = new Conexion();
        Statement st = con.conecta().createStatement();
        
        ResultSet rs = st.executeQuery("SELECT * FROM menu WHERE Cantidad > 0");
         return rs;
        
    }
     public ResultSet  ListaAlmacen() throws SQLException{
        Conexion con = new Conexion();
        Statement st = con.conecta().createStatement();
        
        ResultSet rs = st.executeQuery("SELECT * FROM almacenalimentos WHERE Cantidad > 0");
        
        return rs;
        
    }
     
          public ResultSet  ListaAlmacenFull() throws SQLException{
        Conexion con = new Conexion();
        Statement st = con.conecta().createStatement();
        
        ResultSet rs = st.executeQuery("SELECT * FROM almacenalimentos");
        
        return rs;
        
    }
     
    public ResultSet  BuscarAlimento(String Nombre) throws SQLException{
        Conexion con = new Conexion();
        Statement st = con.conecta().createStatement();
        
        ResultSet rs = st.executeQuery("SELECT * FROM almacenalimentos WHERE Nombre ='"+Nombre+ "'");
        
        return rs;
        
    }
     
    
    public int CantidadAlimetos(String Nombre) throws SQLException{
        Conexion con = new Conexion();
        Statement st = con.conecta().createStatement();
        int cantidad = 0;
        ResultSet rs = st.executeQuery("SELECT Cantidad FROM almacenalimentos WHERE Nombre ='"+Nombre+ "'");
        if (rs.next()){
            cantidad = Integer.valueOf(rs.getString("Cantidad"));
        }
        return cantidad;
    }
     
    
    public int SaldoDisponible(int id) throws SQLException{
        boolean pagar;
        Conexion con = new Conexion();
        Statement st = con.conecta().createStatement();
        int cantidad = 0;
        ResultSet rs = st.executeQuery("SELECT SaldoDisponible FROM comunidaduam WHERE Identificador ='"+id+ "'");
        if (rs.next()){
           cantidad =  Integer.valueOf(rs.getString("SaldoDisponible"));
        }
        return cantidad;
    }
    
    
}
