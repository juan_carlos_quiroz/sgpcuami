/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Select;
import java.sql.SQLException;

/**
 *
 * @author Juan Carlos Quiroz Aguilar
 */
public class ValidaPedido {
    
    public boolean ValidaCantidad(int[] cantidad){
    
        for (int i = 0; i < cantidad.length; i++) {
            if(cantidad[i] > 2){
                return false;
            }
        }       
        return true;
    }
    
    
    public boolean ValidaCosto(int costoTotal) throws SQLException{
        Select s = new Select();
        int saldo;
        saldo = s.SaldoDisponible(2133073);      
        if(saldo < costoTotal){
            return false;
        }
        
        return true;
    }
    
    
}
