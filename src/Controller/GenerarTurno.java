/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.sql.ResultSet;
import Models.*;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Juan Carlos Quiroz Aguilar
 */
public class GenerarTurno {
    
    public String Turno() throws SQLException{
        Select s = new Select();
        String M="";
        String D="";
        String C="";
        ResultSet rs = s.ConsultPedidos();
        int count = 0;
        String turno ="";
        Calendar fecha = new GregorianCalendar();
        int mes = fecha.get(Calendar.MONTH) + 1;
        if(mes < 10){
            M = "0" + mes;
        }else{
            M = String.valueOf(mes);
        }
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
       if(dia < 10){
            D = "0" + dia;
        }else{
            D = String.valueOf(dia);
        }
        
        while (rs.next()) {
            count++;
        }
        
        C = String.valueOf(count + 1);
        
        for (int i = C.length(); i < 4; i++) {
            C = "0" + C;
        }
        
        
        turno = D + "-" + M + "-" + C;
        return(turno);
    }
    
}
